This web app allows a user to be notified via e-mail when a website they wish to track has been updated. Updates are currently monitored via periodic HEAD requests to specified urls, and checking for updates of etag or last-modified headers.

to install dependencies:
    cd server 
    npm i
to run:
    npm run prod

Deployers of this application will also need to supply access to an SMTP server in config.js for e-mail notifications, and should implement SSL for secure authenication.

Future updates will include:
- Encrypted password storge (High priority)
- Account management (change password, delete accounts)
- Fine tuning of notification preferences(frequency and time to check for website updates)
- More robust, and threshold checks for web page differences (currently using last-modified and etag HTTP headers)


