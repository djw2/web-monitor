function deleteSelected(){
	var deleteList = document.getElementsByClassName("deleteBox");
    var resource = "/api/delete?"
    var counter = 0;

	for(var i =0; i < deleteList.length; ++i){
		var checkbox = deleteList[i];
		if(checkbox.checked != false){
			if(counter > 0)
				resource+="&";
            resource +=('id='+checkbox.id);
            counter++;
		}
	}
    if(counter >0 ){
        var xhttp = new XMLHttpRequest();
		xhttp.open('DELETE', resource,true);
		xhttp.send();
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4){
                list();
            }
        };
	}
}

function addMonitor(){
	var new_name = document.getElementById("new_name");
	var new_url = document.getElementById("new_url");
	if(!validURL(new_url.value))
		alert("invalid URL");
	else{
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4){

				if(200 <= this.status &&  this.status< 300){
					
					list();
				}
				else{
					alert("Error, URL invalid. Status: "+this.status);
				}
			}
		};
		xhttp.open("POST", "/api/newMonitor",true);
		xhttp.send(JSON.stringify({"name": new_name.value, "url": new_url.value}));
	}
}

function logout(){
    document.cookie="ssid=0";
    window.location.href = '/index.html';
}


function login(){
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	var obj = {"command":"login","method":"POST","email":email,"password":password};
	if(validEmail(email) && password !== ""){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if(200 <= this.status && this.status < 300){
					  window.location.href = '/account.html'
				}
				else{
					alert("Error. Status: "+this.status);
				}
			}
		};
		xhttp.open(obj.method, "/api/"+obj.command, true);
		//xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xhttp.send(JSON.stringify(obj));
	}
	else{
		alert("Invalid username or password");
	}
	
}

function register(){
	
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	var password_confirm = document.getElementById("confirm_password").value;
	//Client side input checks
	if(password != password_confirm){
		alert("passwords do not match");
	}
	else if(!validEmail(email)){
		alert("Invalid email address");
	}
	else if(!validPassword(password)){
		alert("Invalid password: Must be 8 characters");
	}
	else{
		var xhttp = new XMLHttpRequest();	
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 ) {
				if(200 <= this.status && this.status < 300){
					alert("Success");
					window.location.href = '/index.html';
				}
				else{
					alert("Error. Status: "+this.status);
				}
			}
		}
		xhttp.open("POST", "/api/register", true);
		xhttp.send(JSON.stringify({"email":email,"password":password}));
		}
	}


function list(){
	var table = document.getElementById("table_list_body");
	//table.innerHTML="<tr><td>Name</td><td>URL</td><td>Active?</td></tr>";
	table.innerHTML="";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4) {
			if(200 <= this.status && this.status < 300){
				var array = JSON.parse(xhttp.responseText);
				
				for(var i = 0; i < array.length; ++i){
					var next = array[i];
					var row = document.createElement("tr");

					var nameCell = document.createElement("td");
					nameCell.innerText = next.name;

					var urlCell = document.createElement("td");
					urlCell.innerText = next.url.substr(0,25);

					var activeCell = document.createElement("td");
					var tooltiptext = document.createElement("span");
					console.log(next.last_change);
					if(next.last_change.length >3){
						activeCell.onmouseover = function(){tooltiptext.style.visibility="visible"}
						activeCell.innerText="Active"
						activeCell.style.color="#90ee90";
					}
					else{
						activeCell.innerText="Inactive"
						activeCell.style.color="red";
						activeCell.onmouseover= activeCell.style.cursor="pointer";
						
						activeCell.onclick=function(){alert("URL may not support ETAG or last-modified headers");};
					}
					activeCell.style.fontWeight="bold";
					var checkboxCell = document.createElement("td");
					var checkbox = document.createElement("input");
					checkbox.className="deleteBox";
					checkbox.id= next.rowid;
					checkbox.type="checkbox";
					row.appendChild(nameCell);
					row.appendChild(urlCell);
					row.appendChild(activeCell);
					checkboxCell.appendChild(checkbox);
					row.appendChild(checkboxCell)
					table.appendChild(row);
					/*
					table.innerHTML+=("<tr>"+
					"<td>"+next.name+"</td>"+
					"<td>"+next.url+"...</td>"+
					"<td>"+next.notify+"</td>"+
					'<td><input type="checkbox" class="deleteBox" id="'+next.rowid+'"></input></td>'+
					"</tr>");*/
				}
			}
			else{
				alert("Error. Status: "+this.status);
				window.location.href = '/index.html';
			}
	
		}
	};
	xhttp.open("GET", "/api/list", true);
	xhttp.send();
}

function validEmail(email){
	const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regex.test(email);
}
function validPassword(password){
	return (password.length >= 6)
} 
function validName(){

}
function validURL(url){
	const regex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;
	return regex.test(url);
}
