const branches ={
    "development": {
        "config_id": "development",
        "hostname": "someserver.net",
        "node_port": 8081,
        "database": "./app-db-dev.db",
        "ssl": false,
        "key": "somesecretkey",             //Static key for ease of debugging
        "whitelist": true,
        "blacklist": false,
        "schedule": "0 0 * * * *",
        "countries": ['US', 'CA', 'N/A'],
        "algorithm" : 'aes-256-cbc',
        "root": (process.argv[1].replace('/server/server','/res')),
        "cookie_timeout": 3600000,
        "transporter":{
            "host":"smtp.someserver.net",
            "port":587,
            "secure":false,
            "auth": {
                "user": "someone@someserver.net",
                "pass": "abadpassword",
            },
            "tls":{
                "rejectUnauthorized": false
            },
            "debug":false,
            "authMethod":"LOGIN"
      }
    },

    "production": {
        "config_id": "production",
        "hostname": "someserver.net",
        "node_port": 8080,
        "database": "./app-db-dev.db",
        "ssl": false,
        "key": Math.floor((Math.random() * Math.pow(10,20)) ).toString(),  //Dynamic key on start for security
        "whitelist": true,
        "blacklist": false,
        "schedule": "0 0 * * * *",
        "countries": ['US', 'CA', 'N/A'],
        "algorithm" : 'aes-256-cbc',
        "root": (process.argv[1].replace('/server/server','/res')),
        "cookie_timeout": 3600000,
        "transporter":{
            "host":"smtp.someserver.net",
            "port":587,
            "secure":false,
            "auth": {
                "user": "someone@someserver.net",
                "pass": "abadpassword",
            },
            "tls":{
                "rejectUnauthorized": false
            },
            "debug":false,
            "authMethod":"LOGIN"
      }
    }
}
//default branch
var config = branches.development;    
if(process.argv[2] &&  process.argv[2] === 'prod')
    config = branches.production 
module.exports = {
    config
};
