"use strict";
const {createUser} = require('./model');
const {authenticate} = require('./model');
const {createMonitor} = require('./model');
const {getAllMonitors} = require('./model');
const {removeMonitors} = require('./model');
const { getPostData } = require('./utils');
const {encrypt} = require('./utils');
const {decrypt} = require('./utils');
const {getSSID} = require('./utils');
const {getLastModified} = require('./utils');
const {getETAG} = require('./utils');
const {getHeaders} = require('./utils');



//POST->request: Body->{email:email,password:password}
//POST->response(success): Body->{email:email}, code: 201
//POST->response(fail, already exists): Body->null, code: 409 TODO: more code/handling
//POST->response(fail, bad email address): Body->null, code: 409 TODO: more code/handling
//POST->response(fail, DB/other): Body->null, code: 500 
function registerUser(request,response) {
	//TODO encrypt password before storage 
	const body = getPostData(request);
	
	body.then((jsonstring)=>{
		var email = JSON.parse(jsonstring).email;
		var password = JSON.parse(jsonstring).password;
		let newUser = createUser(email,password);
		
		newUser.then((res) =>{
			response.writeHead(201, { 'Content-Type': 'application/json' })
			response.end();
		}).catch((err) =>{
			response.writeHead(409, { 'Content-Type': 'application/json' })
			response.end();
		});
	}).catch((err)=>{
		response.writeHead(500, { 'Content-Type': 'application/json' })
		response.end();
	});

}

//POST->request: params->empty,Body->{email:email,password:password}
//POST->response(success): Body->{email:email}, code=200
//POST->response(fail, wrong user/pass): Body->{}, code=401
function loginUser(request,response){
	//TODO decrypt password before validating
	const body = getPostData(request);
	body.then((jsonstring)=>{
		var email = JSON.parse(jsonstring).email;
		var password = JSON.parse(jsonstring).password;
		let login = authenticate(email,password);
		login.then((row) =>{
			if(row.email != null && row.email == email){
				console.log("then & match");
				var date = new Date();
				var session_id ="" 
				session_id = session_id.concat(email , ';',request.socket.remoteAddress,';', date.getTime());
				var encrypted = encrypt(session_id);
				response.writeHead(200, { 'Content-Type': 'application/json',
				'Set-Cookie': 'ssid='+encrypted+';SameSite=Lax;Path=/;Expires=' +new Date(date.getTime()+3600000).toGMTString()});
				response.end(JSON.stringify(row));
			}
			else{
				//handle null case
			}
		}).catch((err) =>{
			console.log("catch, login dbBB");
			response.writeHead(409, { 'Content-Type': 'application/json' })
			response.end(JSON.stringify({'message':err}));
		});
	}).catch((err)=>{
		console.log("catch, body");
		response.writeHead(500, { 'Content-Type': 'application/json' })
		response.end(JSON.stringify({'message':err}));
	});
}

//GET->request: params->email=email Body->null cookie->ssid
//GET->response (success): Body->[{monitor1},{monitor2},...] code=200
//GET->response (fail, bad cookie auth): Body->{} code=401
function listMonitors(request, response){
	var ssid = getSSID(request);
	if(ssid.email == undefined){
		response.writeHead(400, { 'Content-Type': 'application/json' })
		response.end();
	}
	else{
		var email = ssid.email;
		const list =  getAllMonitors(email);
		list.then((rows) =>{
			
			response.writeHead(200, { 'Content-Type': 'application/json' })
			response.end(JSON.stringify(rows));
		}).catch((err) =>{
			console.log("Nrows")
			console.log(err);
			response.writeHead(401, { 'Content-Type': 'application/json' })
			response.end();
		});
	}
}

//DELETE->request: params->email=email&mid=mid cookie->ssid
//DELETE->response (success): body->{mid:mid}: code 200
//DELETE->response (fail, bad/no resource): Body->{} code 404
//DELETE->response (fail, bad cookie/auth): Body->{} code: 401
function deleteMonitors(request,response, idArray){
	var ssid = getSSID(request);
	if(ssid.email == undefined){
		console.log("undefined");
		response.writeHead(401, { 'Content-Type': 'application/json' })
		response.end();
	}
	else{
		console.log(idArray);
		const remove = removeMonitors(idArray);
		remove.then(()=>{ //TODO update resolve with ids of removed data
			response.writeHead(200, { 'Content-Type': 'application/json' })
			response.end(); //TODO include removed ids in response
		}).catch((err)=>{
			response.writeHead(401, { 'Content-Type': 'application/json' })
			response.end();
		});

	}
}

//POST->request: params->email=email,name=name,url=url, body->null cookie->ssid
//POST->response (success) body->{name:name,url:url}: code 202
//POST->response (fail, bad url) body->{}: code 400
//POST->response (fail, DB error): body={}: code 500
//POST->response (fail, cookie/auth): body={}: code 401
async function newMonitor(request, response){
	var ssid = getSSID(request);
	if(ssid.email == undefined){
		response.writeHead(401, { 'Content-Type': 'application/json' })
		response.end();
	}
	else{
		const bodyPromise = getPostData(request);
		bodyPromise.then((data)=>{
			var name = JSON.parse(data).name;
			var url = JSON.parse(data).url;
			const headersPromise = getHeaders(url);
			headersPromise.then((headers)=>{
				var etag = getETAG(headers);
				var last_modified = getLastModified(headers);
				last_modified = last_modified +';'+etag;
				//if(last_modified == ';')
				//	last_modified= "Update data or resource unavalible"
				const monitorPromise = createMonitor(name,url, ssid.email, last_modified);
				monitorPromise.then(()=>{ //TODO row inserted, refere to here
					response.writeHead(202, { 'Content-Type': 'application/json' })
					response.end();//TODO respond with row inserted
				}).catch(()=>{
					response.writeHead(500, { 'Content-Type': 'application/json' })
					response.end();
				});
			}).catch((err)=>{
				response.writeHead(400, { 'Content-Type': 'application/json' })
				response.end(err);
			}); //TODO add catch for headers promise

		}).catch(()=>{
			response.writeHead(400, { 'Content-Type': 'application/json' })
			response.end();
		});
	}
}

module.exports = {
	registerUser,
	loginUser ,
	listMonitors,
	newMonitor,
	deleteMonitors
}
