var nodemailer = require("nodemailer");
const {config} = require('./config');
class Mailer{

  constructor() {
      this.transporter = nodemailer.createTransport(config.transporter);
  }
  async monitorUpdateMail(useremail, monitorname, monitorurl){
    var subjecttext = "New website update for "+monitorname;
    var bodytext = "Hello "+useremail+", your monitored website "+monitorname+ " has been updated at "+monitorurl;
    var bodytextHTML = "<p>"+bodytext+"</p>";
    await this.transporter.sendMail({
        from: '"Web Monitor Service" <noreply@'+config.hostname+'>', // sender address
        to: useremail, // list of receivers
        subject: subjecttext, // Subject line
        text: bodytext, // plain text body
        html: bodytextHTML, // html body
      });
  }

  registerConfirmMail(useremail){
    //TODO
  }


}

module.exports = {
  Mailer: Mailer
}
