var cron = require('node-cron');
var {getAllMonitorsGlobal} = require('./model');
const {getLastModified} = require('./utils');
const {getETAG} = require('./utils');
const {getHeaders} = require('./utils');
const {updateLastModified} = require('./model');
const {Mailer} = require('./mailer');
const {config} = require('./config');
//let Mailer = mailer.Mailer;

async function scheduler(){
    var mailer = new Mailer();
    //TEST:  mailer.monitorUpdateMail('davidjwestgate@gmail.com','some_name','some_url');

    //TODO move cron schedule to config file
    var sched = cron.schedule('0 0 * * * *', async() => {
        console.log('running a task every minute');
        var allGlobalMonitorsPromise = getAllMonitorsGlobal();

        allGlobalMonitorsPromise.then(async(rows)=>{
            for(var i = 0;  i < rows.length; ++i){
                var headersPromise = await getHeaders(rows[i].url);  //TODO handle resolve/reject
                var last_change_db =  rows[i].last_change;
                // console.log('last_change_db: '+last_change_db)
                var last_modified_live = getLastModified(headersPromise)+';'+getETAG(headersPromise);
                //console.log('last_modified_live: '+last_modified_live)
                 
                if(last_change_db == last_modified_live){
                    console.log("no change for "+rows[i].name);
                }

                else{
                    console.log("change for "+rows[i].name);
                    var updateLastModifiedPromise = await updateLastModified(rows[i].rowid,last_modified_live);
                    updateLastModifiedPromise.finally(()=>{
                        //TODO: Set email_Are_live in config file
                    // mailer.monitorUpdateMail(rows[i].email,rows[i].name,rows[i].url);
                    });
                       
                } 
            }
        }).catch((err)=>{
            console.log(err);
        });
    });
}
module.exports={
    scheduler
}
