var http = require('http');
var readline = require('readline');
const {config} = require('./config');
const {check_whitelist} = require('./utils');
const {serve_resource} = require('./utils');
const {registerUser} = require('./controller');
const {loginUser} = require('./controller');
const {listMonitors} = require('./controller');
const {newMonitor} = require('./controller');
const {deleteMonitors} = require('./controller');
const {scheduler} = require('./scheduler');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
  });

function server_listener(request, response){
		var ip = request.socket.remoteAddress;
		var whitelist = {'stop':false, 'country':'N/A', 'ip':ip};
		check_whitelist(whitelist);
		
		if(whitelist.stop){
			response.end('Acess Denied');
		}
		
		else{
			var url = new URL(request.url,`http://${request.headers.host}`);
			var path = url.pathname
			var params = new URLSearchParams(url.searchParams);
			//API calls
			if(path.startsWith("/api")){
				switch(request.socket.parser.incoming.method){
					case "GET":
						if(path.endsWith("/list"))
							listMonitors(request,response);
						else if(path.endsWith("/logout"))
							logoutUser(request,response);
						break;
					case "POST":
						if(path.endsWith("/register"))
							registerUser(request,response);
						else if(path.endsWith("/login"))
							loginUser(request,response);
						else if(path.endsWith("/newMonitor"))
							newMonitor(request,response);
						break;
					case "PUT":
						break;
					case "DELETE":
						if(path.endsWith("/delete")){
							var idArray = JSON.parse('['+params.getAll('id')+']');
							deleteMonitors(request,response,idArray);
						}						
						break;	
				}
			}		  
			else{
				serve_resource(request,response,whitelist);
			}
	}
}
scheduler();
var server = http.createServer(server_listener);
server.listen(config.node_port);

rl.question("Server running (Enter to stop)\n",(answer) =>{
	server.close();
	process.exit(1);
});


